#ifndef LRUMC_FEATURES_H_
#define LRUMC_FEATURES_H_

#include <string>

#include <elm/data/Array.h>

#include <otawa/proc/AbstractFeature.h>
#include <otawa/icat3/features.h>

namespace lrumc {

// MC Program Model Building

class MCProgramModel;

extern otawa::p::feature PROGRAM_MODEL_BUILDING_FEATURE;
//hook: access
extern otawa::p::id<MCProgramModel*> PROGRAM_MODEL;

//hook: workspace
extern otawa::p::id<MCProgramModel*> GLOBAL_MODEL;
//hook: LBlockSet
extern otawa::p::feature SET_SLICING_PROGRAM_MODEL_FEATURE;
extern otawa::p::id<elm::AllocArray<MCProgramModel*> > SET_SLICED_PROGRAM_MODEL;
//hook: LBlock
extern otawa::p::feature LBLOCK_SLICING_PROGRAM_MODEL_FEATURE;
extern otawa::p::id<elm::AllocArray<elm::AllocArray<MCProgramModel*> > > LBLOCK_SLICED_PROGRAM_MODEL;

// MC Program Model Slicing

enum class ProgramSlicing
{
	SET_SLICING,
	CACHE_BLOCK_SLICING,
	ACCESS_SLICING
};

extern otawa::p::id<ProgramSlicing> PROGRAM_SLICING;

// MC Cache Model Building
enum class CacheModelKind
{
	YOUNGER_SET,
	YOUNGER_SET_BOUNDED_BITVECTOR,
	YOUNGER_SET_BITVECTOR,
	YOUNGER_SET_CLOSED_BITVECTOR,
	MAP_BLOCK_AGE,
	QUEUE
};

extern otawa::p::id<CacheModelKind> CACHE_MODEL_KIND;


// Model Checking Analysis

extern otawa::p::feature MODEL_CHECKING_ANALYSIS_FEATURE;
extern otawa::p::id<elm::String> NUXMV_PATH;
extern otawa::p::id<elm::String> NUXMV_WORKSPACE_PATH;


// Classification printing
extern otawa::p::id<bool> CLASSIFICATION_TO_FILE;
extern otawa::p::id<elm::sys::Path> CLASSIFICATION_PATH;

} // namespace lrumc

namespace otawa
{
	template <> void from_string(const elm::string& s, lrumc::CacheModelKind& v);
	template <> void from_string(const elm::string& s, lrumc::ProgramSlicing& v);
} // namespace otawa

#endif // LRUMC_FEATURES_H
