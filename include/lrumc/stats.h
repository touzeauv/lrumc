#ifndef LRUMC_STATS_H
#define LRUMC_STATS_H

#include <otawa/stats/BBStatCollector.h>
#include <otawa/cfg/CFG.h>
#include <otawa/icache/features.h>

namespace lrumc
{

class StatCollector: public otawa::BBStatCollector {
public:
	StatCollector(otawa::WorkSpace *ws, cstring name, cstring unit)
	: otawa::BBStatCollector(ws), _name(name), _unit(unit) {}

	virtual cstring name(void) const override { return _name; }
	virtual cstring unit(void) const override { return _unit; }
	virtual bool isEnum(void) const override { return false; }
	virtual const cstring valueName(int) override { return ""; }

	virtual int total(otawa::BasicBlock *bb) override;

	void collect(otawa::BBStatCollector::Collector& collector, otawa::BasicBlock *b, const otawa::ContextualPath& ctx) override;

	virtual int mergeContext(int v1, int v2) override { return v1 + v2; }
	virtual int mergeAgreg(int v1, int v2) override { return v1 + v2; }

protected:
	virtual bool keep(const otawa::icache::Access* access) const;
private:
	int count(otawa::BasicBlock* bb) const;
	cstring _name, _unit;
};


class AHCollector: public StatCollector
{
public:
	AHCollector(otawa::WorkSpace *ws): StatCollector(ws, "L1 Instruction Cache AH classified Accesses", "cache access(es)")  { }

protected:
	virtual bool keep(const otawa::icache::Access* access) const override;
};


class AMCollector: public StatCollector
{
public:
	AMCollector(otawa::WorkSpace *ws): StatCollector(ws, "L1 Instruction Cache AM classified Accesses", "cache access(es)")  { }

protected:
	virtual bool keep(const otawa::icache::Access* access) const override;
};


class NCCollector: public StatCollector
{
public:
	NCCollector(otawa::WorkSpace *ws): StatCollector(ws, "L1 Instruction Cache NC classified Accesses", "cache access(es)")  { }

protected:
	virtual bool keep(const otawa::icache::Access* access) const override;
};

} // namespace lrumc

#endif // LRUMC_STATS_H
