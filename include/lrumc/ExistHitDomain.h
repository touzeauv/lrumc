#ifndef LRUMC_EXIST_HIT_DOMAIN_H_
#define LRUMC_EXIST_HIT_DOMAIN_H_

#include <elm/io/Output.h>
#include <otawa/hard/Cache.h>
#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>
#include <otawa/cfg/BasicBlock.h>

namespace lrumc {

class ExistHitDomain {
public:
	using t = otawa::icat3::ACS;

	ExistHitDomain(
		const otawa::icat3::LBlockCollection& coll,
		int set,
		const t *init);

	t init();
	t bot();
	void copy(t& d, const t& s);

	t join(const t& d1, const t& d2);

	bool equals(const t& v1, const t& v2);

	void set(t& d, const t& s);

	void dump(elm::io::Output& out, t c);


	void update(otawa::Block *bb, t& a);
	void update(otawa::Edge *e, t& a);
private:
	void fetch(t& a, const otawa::icat3::LBlock *lb);
	void update(const otawa::Bag<otawa::icache::Access>& os, t& a);
	void update(const otawa::icache::Access& o, t& a);

	const int _count;
	const int _ways;
	const t _bot, _top;
	const t& _init;
	otawa::hard::Cache::set_t _set;
	const otawa::icat3::LBlockCollection& _coll;
};

} // namespace lrumc

#endif // LRUMC_EXIST_HIT_DOMAIN_H_
