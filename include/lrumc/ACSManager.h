#ifndef LRUMC_ACS_MANAGER_H_
#define LRUMC_ACS_MANAGER_H_

#include <otawa/icat3/features.h>
#include <otawa/cfg/features.h>

#include <lrupreanalysis/features.h>

namespace lrumc
{

class CacheState;

class ACSManager
{
public:
	ACSManager(otawa::WorkSpace* ws);
	~ACSManager(void);
	void start(otawa::Block* b);
	void update(const otawa::icache::Access& acc);
	int mustAge(const otawa::icat3::LBlock* b);
	int mayAge(const otawa::icat3::LBlock* b);
	int existHitAge(const otawa::icat3::LBlock* b);
	int existMissAge(const otawa::icat3::LBlock* b);
	void print(const otawa::icat3::LBlock* b, otawa::io::Output& out);
private:
	CacheState& use(int set);

	lrupreanalysis::may_must::ACSManager _underlyingManager;
	const otawa::icat3::LBlockCollection& _coll;
	elm::Vector<int> _used;
	otawa::Block* _b;
	CacheState** _states;

	bool _hasExistHit;
	bool _hasExistMiss;
};

} // namespace lrumc

#endif // LRUMC_ACS_MANAGER_H_
