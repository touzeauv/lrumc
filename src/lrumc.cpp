#include <otawa/proc/ProcessorPlugin.h>

namespace lrumc {

using namespace elm;
using namespace otawa;

class Plugin: public ProcessorPlugin {
public:
	Plugin(void): ProcessorPlugin("lrumc", Version(1, 0, 0), OTAWA_PROC_VERSION) { }
};

} // namespace lrumc

lrumc::Plugin lrumc_plugin;
ELM_PLUGIN(lrumc_plugin, OTAWA_PROC_HOOK);

