#ifndef LRUMC_CACHE_STATE_H_
#define LRUMC_CACHE_STATE_H_

#include "EH/ExistHitDomain.h"
#include "EM/ExistMissDomain.h"

namespace lrumc
{

class CacheState
{
public:
	CacheState(const otawa::icat3::LBlockCollection& coll, int set) :
			_existHitDomain(coll, set, nullptr),
			_existMissDomain(coll, set, nullptr),
			_existHitState(nullptr),
			_existMissState(nullptr)
	{
	}

	~CacheState()
	{
		if(_existHitState)
			delete _existHitState;
		if(_existMissState)
			delete _existMissState;
	}

	ExistHitDomain _existHitDomain;
	ExistMissDomain _existMissDomain;

	typename ExistHitDomain::t* _existHitState;
	typename ExistMissDomain::t* _existMissState;
};

} // namespace lrumc

#endif // LRUMC_CACHE_STATE_H_
