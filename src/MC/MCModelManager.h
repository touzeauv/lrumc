#ifndef LRUMC_MC_MODEL_MANAGER_H
#define LRUMC_MC_MODEL_MANAGER_H

#include <otawa/icache/features.h>

#include <lrumc/features.h>

#include "MCProgramModelSlicing.h"

namespace lrumc
{

extern otawa::p::id<MCProgramModel*> ACCESS_MODEL;

class MCModelManager
{
public:
	MCModelManager(ProgramSlicing programSlicing, int sets, const MCProgramModel* globalModel, const otawa::hard::Cache* cache);
	~MCModelManager();

	MCProgramModel& getModel(otawa::icache::Access& access);
private:
	ProgramSlicing _programSlicing;
};

} // namespace lrumc

#endif // LRUMC_MODEL_MANAGER_H
