#include "MCProgramModel.h"

#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>

using namespace otawa;

namespace lrumc {

MCProgramModel::NodeID MCProgramModel::createNode(const otawa::icache::Access* access, const Block* block)
{
	const icat3::LBlock* lblock = (access ? icat3::LBLOCK(access) : nullptr);

	BlockID blockId = createBlockIfNeeded(lblock);
	NodeID nodeId = _nodes.size();

	MCProgramModelNode* node = new MCProgramModelNode(nodeId, blockId, access, block);
	_nodes.push_back(node);
	_nodeIdMap.insert(std::make_pair(access, nodeId));
	return nodeId;
}

MCProgramModel::~MCProgramModel()
{
	for(auto node : _nodes)
		delete node;
}

void MCProgramModel::setEntry(NodeID entry)
{
	_entry = entry;
}

MCProgramModel::NodeID MCProgramModel::entry() const
{
	return _entry;
}

int MCProgramModel::nbBlocks() const
{
	return _blocks.size();
}

void MCProgramModel::printSMV(elm::io::Output& output) const
{
	// Define the main module (program model)
	output << "MODULE main\n";

// Define variables (state space)
	output << "\tVAR\n";

	// cache is the model of the choosen cache set
	output << "\t\tcache : cache_set(next(block));\n";

	// state is the current node in the graph
	output << "\t\tstate : {-1, ";
	bool first_loop = true;
	for(const auto& node : _nodes) {
		if(first_loop)
			first_loop = false;
		else
			output << ", ";
		output << node->id();
	}
	output << "};\n";

	// block accessed by the current node (or -1 if no access)
	output << "\t\tblock : -1.." << nbBlocks() - 1 << ";\n";

// Describe assignments (transitions)
	output << "\tASSIGN\n";
	output << "\t\tinit(state) := " << _entry << ";\n";

	output << "\t\tnext(state) :=\n";
	output << "\t\t\tcase\n";

	for(const auto& source : _nodes) {
		output << "\t\t\t\t(state = " << source->id() << ") : {";

		bool first_loop = true;
		for(const auto& target : *source) {
			if(first_loop)
				first_loop = false;
			else
				output << ", ";
			output << target->id();
		}

		// if node has no child nuXmv complains. Thus, we artificially add the final state -1 as a child.
		if(first_loop) {
			output << "-1";
		}
		output << "};\n";
	}

	// Special case for the final state which loop forever
	output << "\t\t\t\t(state = -1) : {-1};\n";

	output << "\t\t\tesac;\n";


	output << "\t\tblock :=\n";
	output << "\t\t\tcase\n";
	for(const auto& node : _nodes) {
		output << "\t\t\t\tstate = " << node->id() << " : ";
		output << node->blockId();
		output << ";\n";
	}
	// Special case : the final node (-1) does not make any access
	output << "\t\t\t\tstate = -1 : -1;\n";
	output << "\t\t\tesac;\n";
}

void MCProgramModel::printDOT(elm::io::Output& output) const
{
	output << "digraph CacheCFG {\n";
	for(const auto& node : _nodes) {
		output << "\tnode"
		       << node->id()
		       << " [shape=record, label=\"{{mc_id: "
		       << node->id()
		       << "}|{mc_block: "
		       << node->blockId();
		BlockID blockId = node->blockId();
		if(blockId != -1) {
			const icat3::LBlock* lblock = _blocks[node->blockId()];
			const icache::Access* access = node->access();
			output << "}|{kind: ";
			if(access->kind() == otawa::icache::FETCH)
				output << "FETCH";
			else if(access->kind() == otawa::icache::PREFETCH)
				output << "PREFETCH";

			output << "}|{set: "
			       << lblock->set()
			       << ", index: "
			       << lblock->index()
			       << "}|{@:"
			       << access->address();
		}
		output << "}}\"];\n";
	}
	output << "\n";
	for(const auto& source : _nodes) {
		for(const auto& target : *source) {
			output << "\tnode"
			       << source->id()
			       << " -> node"
			       << target->id()
			       << ";\n";
		}
	}

	output << "}\n";
}



MCProgramModel::BlockID MCProgramModel::createBlockIfNeeded(const otawa::icat3::LBlock* lblock)
{
	if(lblock == nullptr)
		return -1;
	std::map<const icat3::LBlock*, BlockID>::const_iterator it = _blockIdMap.find(lblock);
	if(it != _blockIdMap.end())
		return it->second;

	BlockID id = _blocks.size();
	_blocks.push_back(lblock);
	_blockIdMap.insert(std::make_pair(lblock, id));
	return id;
}

const MCProgramModelNode* MCProgramModel::getNode(NodeID id) const
{
	return _nodes[id];
}

MCProgramModelNode* MCProgramModel::getNode(NodeID id)
{
	return _nodes[id];
}

MCProgramModel::NodeID MCProgramModel::getAccessId(const otawa::icache::Access* access) const
{
	return _nodeIdMap.at(access);
}

MCProgramModel::BlockID MCProgramModel::getBlockId(const otawa::icat3::LBlock* block) const
{
	return _blockIdMap.at(block);
}

MCProgramModel::node_iterator MCProgramModel::begin()
{
	return _nodes.begin();
}

MCProgramModel::node_iterator MCProgramModel::end()
{
	return _nodes.end();
}

MCProgramModel::node_const_iterator MCProgramModel::begin() const
{
	return _nodes.begin();
}

MCProgramModel::node_const_iterator MCProgramModel::end() const
{
	return _nodes.end();
}

} // namespace lrumc
