#include <lrumc/stats.h>

#include <otawa/icat3/features.h>

namespace lrumc
{

int StatCollector::total(otawa::BasicBlock *bb)
{
	return count(bb);
}

void StatCollector::collect(otawa::BBStatCollector::Collector& collector, otawa::BasicBlock *b, const otawa::ContextualPath& ctx) {
	if(!b->isBasic())
		return;
	otawa::BasicBlock *bb = b->toBasic();

	collector.collect(bb->address(), bb->size(), count(bb), ctx);
}

int StatCollector::count(otawa::BasicBlock *bb) const
{
	const otawa::Bag<otawa::icache::Access>& bag = otawa::icache::ACCESSES(bb);
	int total = 0;
	for(int i = 0; i < bag.size(); ++i)
		if(keep(&bag[i]))
			++total;

	for(otawa::Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
		const otawa::Edge* e = *edgeIter;
		const otawa::Bag<otawa::icache::Access>& bag = otawa::icache::ACCESSES(e);

		for(int i = 0; i < bag.size(); ++i)
			if(keep(&bag[i]))
				++total;
	}

	return total;
}

bool StatCollector::keep(const otawa::icache::Access* access) const
{
	return access->hasProp(otawa::icat3::CATEGORY);
}


bool AHCollector::keep(const otawa::icache::Access* access) const
{
	return access->hasProp(otawa::icat3::CATEGORY) && otawa::icat3::CATEGORY(*access) == otawa::icat3::AH;
}


bool AMCollector::keep(const otawa::icache::Access* access) const
{
	return access->hasProp(otawa::icat3::CATEGORY) && otawa::icat3::CATEGORY(*access) == otawa::icat3::AM;
}


bool NCCollector::keep(const otawa::icache::Access* access) const
{
	return access->hasProp(otawa::icat3::CATEGORY) && otawa::icat3::CATEGORY(*access) == otawa::icat3::NC;
}



} // namespace lrumc
