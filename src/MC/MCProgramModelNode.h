#ifndef LRUMC_MC_PROGRAM_MODEL_NODE_H_
#define LRUMC_MC_PROGRAM_MODEL_NODE_H_

#include <vector>

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/features.h>

namespace lrumc {

class MCProgramModel;

class MCProgramModelNode
{
public:
	using NodeID = int;
	using BlockID = int;

	using iterator = std::vector<MCProgramModelNode*>::iterator;
	using const_iterator = std::vector<MCProgramModelNode*>::const_iterator;

	MCProgramModelNode(NodeID id, BlockID blockId, const otawa::icache::Access* access, const otawa::Block* block);
	void addSuccessor(MCProgramModelNode* node);

	const otawa::icache::Access* access() const {return _access;}
	const otawa::Block* block() const {return _block;}
	NodeID id() const {return _id;}
	BlockID blockId() const {return _blockId;}

	iterator begin() {return _successors.begin();}
	const_iterator begin() const {return _successors.begin();}
	iterator end() {return _successors.end();}
	const_iterator end() const {return _successors.end();}
private:
	NodeID _id;
	BlockID _blockId;
	const otawa::icache::Access* _access;
	const otawa::Block* _block;

	std::vector<MCProgramModelNode*> _successors;
};

} // namespace lrumc

#endif // LRUMC_MC_PROGRAM_MODEL_NODE_H_
