#include "MCProgramModelBuilding.h"

using namespace otawa;

namespace lrumc {

MCProgramModel* buildProgramModel(const CFGCollection* cfgs)
{
	MCProgramModel* result = new MCProgramModel();

	BlockMap blockMap;
	EdgeMap edgeMap;

	for(CFGCollection::Iter cfgIter(cfgs); cfgIter(); ++cfgIter) {
		const CFG* cfg = *cfgIter;
		for(CFG::BlockIter blockIter(cfg->vertices()); blockIter(); ++blockIter) {
			const Block* block = *blockIter;

			if(block->isBasic()) {
				const BasicBlock* bb = const_cast<Block*>(block)->toBasic();
				buildBasicBlockModel(result, bb, blockMap);
			}
			else if (block->isSynth()){
				const SynthBlock* sb = const_cast<Block*>(block)->toSynth();
				buildSynthBlockModel(result, sb, blockMap);
			}
			else if (block->isEnd()) {
				buildEndBlockModel(result, block, blockMap);
			}
			else {
				ASSERT(false);
			}
		}
		for(CFG::BlockIter blockIter(cfg->vertices()); blockIter(); ++blockIter) {
			const Block* block = *blockIter;
			for(Block::EdgeIter edgeIter(block->outs()); edgeIter(); ++edgeIter) {
				const Edge* e = *edgeIter;
				buildEdgeModel(result, e, edgeMap);
			}
		}
		connectAccessModels(cfg, blockMap, edgeMap);
	}
	connectCFGModels(cfgs, blockMap);

	result->setEntry(blockMap.at(cfgs->entry()->entry()).first->id());

	return result;
}

void buildBasicBlockModel(MCProgramModel* model, const BasicBlock* bb, BlockMap& blockMap)
{
	const Bag<icache::Access>& bag = icache::ACCESSES(bb);
	blockMap.insert(std::make_pair(bb, buildAccessBagModel(model, bag, bb)));
}

void buildSynthBlockModel(MCProgramModel* model, const SynthBlock* sb, BlockMap& blockMap)
{
	MCProgramModel::NodeID idFirst = model->createNode(0, sb);
	MCProgramModelNode* first = model->getNode(idFirst);
	MCProgramModel::NodeID idLast = model->createNode(0, sb);
	MCProgramModelNode* last = model->getNode(idLast);
	blockMap.insert(std::make_pair(sb, std::make_pair(first, last)));
}

void buildEndBlockModel(MCProgramModel* model, const Block* b, BlockMap& blockMap)
{
	MCProgramModel::NodeID idNode = model->createNode(0, b);
	MCProgramModelNode* node = model->getNode(idNode);
	blockMap.insert(std::make_pair(b, std::make_pair(node, node)));
}

void buildEdgeModel(MCProgramModel* model, const Edge* e, EdgeMap& edgeMap)
{
	const Bag<icache::Access>& bag = icache::ACCESSES(e);
	edgeMap.insert(std::make_pair(e, buildAccessBagModel(model, bag, e->source())));
}

std::pair<MCProgramModelNode*, MCProgramModelNode*>
buildAccessBagModel(MCProgramModel* model, const Bag<icache::Access>& bag, const Block* block)
{
	MCProgramModelNode *first = 0, *last = 0, *previous = 0, *current = 0;
	for(int i = 0; i < bag.size(); ++i) {
		const icache::Access* access = &bag[i];
		MCProgramModel::NodeID id = model->createNode(access, block);
		current = model->getNode(id);

		if(previous)
			previous->addSuccessor(current);
		if(i == 0)
			first = current;
		if(i == bag.size() - 1)
			last = current;

		previous = current;
	}
	if(!current) {
		MCProgramModel::NodeID idNode = model->createNode(0, block);
		MCProgramModelNode* node = model->getNode(idNode);
		first = node;
		last = node;
	}

	return std::make_pair(first, last);
}

void connectAccessModels(
	const CFG* cfg,
	const BlockMap& blockMap,
	const EdgeMap& edgeMap)
{
	for(CFG::BlockIter blockIter(cfg->vertices()); blockIter(); ++blockIter) {
		const Block* source = *blockIter;
		for(Block::EdgeIter edgeIter(source->outs()); edgeIter(); ++edgeIter) {
			const Edge* e = *edgeIter;
			const Block* sink = e->sink();
			blockMap.at(source).second->addSuccessor(edgeMap.at(e).first);
			edgeMap.at(e).second->addSuccessor(blockMap.at(sink).first);
		}
	}
}

void connectCFGModels(
	const otawa::CFGCollection* cfgs,
	const BlockMap& blockMap)
{
	for(CFGCollection::Iter cfgIter(cfgs); cfgIter(); ++cfgIter) {
		const CFG* cfg = *cfgIter;
		MCProgramModelNode* entry = blockMap.at(cfg->entry()).first;
		MCProgramModelNode* exit = blockMap.at(cfg->exit()).second;
		for(CFG::CallerIter callerIter(cfg->callers()); callerIter(); ++callerIter) {
			const SynthBlock* caller = *callerIter;
			blockMap.at(caller).first->addSuccessor(entry);
			exit->addSuccessor(blockMap.at(caller).second);
		}
	}
}

} // namespace lrumc
