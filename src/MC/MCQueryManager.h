#ifndef LRUMC_MC_QUERY_MANAGER_H
#define LRUMC_MC_QUERY_MANAGER_H

#include <queue>

#include <otawa/icache/features.h>

#include <lrumc/features.h>

#include "MCProgramModelSlicing.h"
#include "MCCheck.h"

namespace lrumc
{

extern otawa::p::id<MCProgramModel*> ACCESS_MODEL;

class MCQueryManager
{
public:
	using Query = std::pair<otawa::icache::Access*, Check>;

	class QueryOrder
	{
	public:
		bool operator() (const Query& lhs, const Query& rhs);
	};

	MCQueryManager(ProgramSlicing slicing, CacheModelKind cacheModelKind, int sets, const otawa::hard::Cache* cache);
	~MCQueryManager();

	void addTask(Query auery);
	bool isEmpty() const;
	Query currentTask() const;
	bool refreshNeeded() const;

	void nextTask();

private:
	std::priority_queue<Query, std::vector<Query>, QueryOrder> _worklist;
	ProgramSlicing _slicing;
	CacheModelKind _cacheModelKind;
	std::vector<MCProgramModel*> _setModels;
	const otawa::hard::Cache* _cache;
	otawa::icache::Access* _lastTask;
};

} // namespace lrumc

#endif // LRUMC_MC_QUERY_MANAGER_H
