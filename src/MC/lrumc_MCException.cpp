#include "MCException.h"

namespace lrumc
{

MCException::MCException(const std::string message) : _message(message)
{
}

MCException::~MCException() throw()
{
}

const char* MCException::what() const throw()
{
	return _message.c_str();
}

} // namespace lrumc

