#include "MCCacheModel.h"

#include <sstream>

namespace lrumc
{

MCCacheModel::MCCacheModel(
		CacheModelKind kind,
		int ways,
		int numberOfBlocks,
		int trackedBlock) :
	_kind(kind),
	_ways(ways),
	_numberOfBlocks(numberOfBlocks),
	_trackedBlock(trackedBlock)
{
}

void MCCacheModel::printSMV(elm::io::Output& output, Check check)
{
	if(_kind == CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR) {
		output << "MODULE cache_set(block)\n";


		//////// VARIABLE DECLARATION ////////
		output << "\tVAR\n";

		// Variable age stores the cardinal of the younger set
		// _ways is a special value used when the tracked block is outside the cache
		output << "\t\tage : 0.." << _ways << ";\n";
		// Variable younger_set is the younger_set abstraction.
		output << "\t\tblocks_in : array 0.." << _numberOfBlocks - 1 << " of boolean;\n";


		// Helpers definition
		output << "\tDEFINE\n";
		// The tracked block
		output << "\t\ttracked_block := " << _trackedBlock << ";\n";
		// Boolean indicating that the tracked block is the accessed block
		output << "\t\thit := (tracked_block = block);\n";
		// Special boolean used when there is no block accessed
		output << "\t\tempty_block := (block = -1);\n";


		//////// TRANSITION DEFINITION ////////
		output << "\tASSIGN\n";

		// Start with empty cache
		output << "\t\tinit(age) := " << _ways << ";\n";
		for(int i = 0; i <= _numberOfBlocks-1; ++i)
			output << "\t\tinit(blocks_in[" << i << "]) := FALSE;\n";

		// Transition for the cardinal of the younger set
		output << "\t\tnext(age) :=\n";
		output << "\t\t\tcase\n";
		// Does not change if no access is made
		output << "\t\t\t\tempty_block : age;\n";
		// on a hit, younger set is empty
		output << "\t\t\t\thit : 0;\n";
		// if the block accessed is older than the tracked block, cardinal is incremented (is not already maximal)
		for(int i = 0; i < _ways; ++i)
			output << "\t\t\t\t!hit & !blocks_in[block] & age = " << i << " : " << i+1 << ";\n";
		// if cardinal is maximum, keep it
		output << "\t\t\t\tTRUE : age;\n";
		output << "\t\t\tesac;\n";



		// Transition for the younger set
		for(int i = 0; i <= _numberOfBlocks - 1; ++i) {
			output << "\t\tnext(blocks_in[" << i << "]) :=\n";
			output << "\t\t\tcase\n";
			// Does not change if no access is made
			output << "\t\t\t\tempty_block : blocks_in[" << i << "];\n";
			// Reset younger set on hit and eviction of the tracked block
			output << "\t\t\t\thit | (age = " << _ways - 1 << " & next(age) = " << _ways << "): FALSE;\n";
			// Block is accessed, insert it into the younger set
			output << "\t\t\t\tnext(age) != " << _ways << " & block = " << i << " : TRUE;\n";
			// Otherwise, keep the old value
			output << "\t\t\t\tTRUE : blocks_in[" << i << "];\n";
			output << "\t\t\tesac;\n";
		}
	}
	else if(_kind == CacheModelKind::YOUNGER_SET_BITVECTOR) {
		output << "MODULE cache_set(block)\n";

		//////// VARIABLE DECLARATION ////////
		output << "\tVAR\n";

		// The bitvector indicating which blocks are younger than the tracked block
		output << "\t\tyounger_set : array 0.." << _numberOfBlocks - 1 << " of boolean;\n";


		// Helpers definition
		output << "\tDEFINE\n";
		// The tracked block
		output << "\t\ttracked_block := " << _trackedBlock << ";\n";
		// Boolean indicating that the tracked block is the accessed block
		output << "\t\thit := (tracked_block = block);\n";
		// Special boolean used when there is no block accessed
		output << "\t\tempty_block := (block = -1);\n";


		//////// TRANSITION DEFINITION ////////
		output << "\tASSIGN\n";

		for(int i = 0; i <= _numberOfBlocks - 1; ++i)
			output << "\t\tinit(younger_set[" << i <<"]) := TRUE;\n";

		// Transition for the younger set
		for(int i = 0; i <= _numberOfBlocks - 1; ++i) {
			output << "\t\tnext(younger_set[" << i << "]) :=\n";
			output << "\t\t\tcase\n";
			// Does not change if no access is made
			output << "\t\t\t\tempty_block : younger_set[" << i << "];\n";
			// Reset younger set on hit
			output << "\t\t\t\thit : FALSE;\n";
			// Block is accessed, insert it into the younger set
			output << "\t\t\t\tblock = " << i << " : TRUE;\n";
			// Otherwise, keep the old value
			output << "\t\t\t\tTRUE : younger_set[" << i << "];\n";
			output << "\t\t\tesac;\n";
		}
	}
	else if(_kind == CacheModelKind::YOUNGER_SET_CLOSED_BITVECTOR) {
		// elements of the bitvector are allowed to take value FALSE at anytime, because it does not change the hit classification
		std::string closingTerm = (check == Check::ALWAYS_HIT ? "FALSE" : "TRUE");
		output << "MODULE cache_set(block)\n";

		//////// VARIABLE DECLARATION ////////
		output << "\tVAR\n";

		// The bitvector indicating which blocks are younger than the tracked block
		output << "\t\tyounger_set : array 0.." << _numberOfBlocks - 1 << " of boolean;\n";


		// Helpers definition
		output << "\tDEFINE\n";
		// The tracked block
		output << "\t\ttracked_block := " << _trackedBlock << ";\n";
		// Boolean indicating that the tracked block is the accessed block
		output << "\t\thit := (tracked_block = block);\n";
		// Special boolean used when there is no block accessed
		output << "\t\tempty_block := (block = -1);\n";


		//////// TRANSITION DEFINITION ////////
		output << "\tASSIGN\n";

		for(int i = 0; i <= _numberOfBlocks - 1; ++i)
			output << "\t\tinit(younger_set[" << i <<"]) := {TRUE, " << closingTerm.c_str() << "};\n";

		// Transition for the younger set
		for(int i = 0; i <= _numberOfBlocks - 1; ++i) {
			output << "\t\tnext(younger_set[" << i << "]) :=\n";
			output << "\t\t\tcase\n";
			// Does not change if no access is made
			output << "\t\t\t\tempty_block : {younger_set[" << i << "], " << closingTerm.c_str() << "};\n";
			// Reset younger set on hit
			output << "\t\t\t\thit : {FALSE, " << closingTerm.c_str() << "};\n";
			// Block is accessed, insert it into the younger set
			output << "\t\t\t\tblock = " << i << " : {TRUE, " << closingTerm.c_str() << "};\n";
			// Otherwise, keep the old value
			output << "\t\t\t\tTRUE : {younger_set[" << i << "], " << closingTerm.c_str() << "};\n";
			output << "\t\t\tesac;\n";
		}
	}
	else if(_kind == CacheModelKind::YOUNGER_SET) {
		output << "MODULE cache_set(block)\n";

		output << "\tVAR\n";

		output << "\t\tage : 0.." << _ways << ";\n";
		output << "\t\tblocks_in : array 1.." << _ways - 1 << " of -1.." << _numberOfBlocks - 1 << ";\n";


		output << "\tDEFINE\n";
		output << "\t\tselected_block := " << _trackedBlock << ";\n";
		output << "\t\thit := (selected_block = block);\n";
		output << "\t\tempty_block := (block = -1);\n";


		output << "\tASSIGN\n";

		output << "\t\tinit(age) := " << _ways << ";\n";
		for(int i = 1; i <= _ways - 1; ++i)
			output << "\t\tinit(blocks_in[" << i << "]) := -1;\n";

		output << "\t\tnext(age) :=\n";
		output << "\t\t\tcase\n";
		output << "\t\t\t\tempty_block : age;\n";
		output << "\t\t\t\thit : 0;\n";
		for(int i = 0; i < _ways; ++i) {
			output << "\t\t\t\t!hit";
			for(int j = 1; j <= i; ++j)
				output << " & blocks_in[" << j << "] != block";
			output << " & age = " << i << " : " << i+1 << ";\n";
		}
		output << "\t\t\t\tTRUE : age;\n";
		output << "\t\t\tesac;\n";


		for(int i = 1; i <= _ways - 1; ++i) {
			output << "\t\tnext(blocks_in[" << i << "]) :=\n";
			output << "\t\t\tcase\n";
			output << "\t\t\t\tempty_block : blocks_in[" << i << "];\n";
			output << "\t\t\t\thit | (age = " << _ways - 1 << " & next(age) = " << _ways << ") : -1;\n";
			output << "\t\t\t\tage = " << i-1 << " & next(age) = " << i << " : block;\n";
			output << "\t\t\t\tTRUE : blocks_in[" << i << "];\n";
			output << "\t\t\tesac;\n";
		}
	}
	else if(_kind == CacheModelKind::MAP_BLOCK_AGE) {
		output << "MODULE cache_set(block)\n";

		output << "\tVAR\n";

		output << "\t\tages : array -1.." << _numberOfBlocks << " of 0.." << _ways << ";\n";

		output << "\tASSIGN\n";

		for(int i = -1; i <= _numberOfBlocks; ++i)
			output << "\t\tinit(ages[" << i << "]) := " << _ways << ";\n";

		output << "\t\tnext(ages[-1]) := ages[-1];\n";
		for(int i = 0; i <= _numberOfBlocks; ++i) {
			output << "\t\tnext(ages[" << i << "]) :=\n";
			output << "\t\t\tcase\n";
			output << "\t\t\t\t(block = -1) : ages[" << i << "];\n";
			output << "\t\t\t\t(block = " << i << ") : 0;\n";
			output << "\t\t\t\t(ages[" << i << "] = " << _ways << ") : " << _ways << ";\n";
			output << "\t\t\t\t(ages[" << i << "] < ages[block]) : ages[" << i << "] + 1;\n";
			output << "\t\t\t\tTRUE : ages[" << i << "];\n";
			output << "\t\t\tesac;\n";
		}
	}
	else if(_kind == CacheModelKind::QUEUE) {
		output << "MODULE cache_set(block)\n";

		output << "\tVAR\n";

		output << "\t\tlines : array 0.." << _ways - 1 << " of -1.." << _numberOfBlocks << ";\n";

		output << "\tASSIGN\n";

		for(int i = 0; i < _ways; ++i)
			output << "\t\tinit(lines[" << i << "]) := -1;\n";

		output << "\t\tnext(lines[0]) := (block = -1) ? lines[0] : block;\n";
		for(int i = 1; i < _ways; ++i) {
			output << "\t\tnext(lines[" << i << "]) := (block = -1) ? lines[" << i << "] :\n";
			output << "\t\t\tcase\n";
			for(int j = 0; j < i; ++j)
				output << "\t\t\t\t(block = lines[" << j << "]) : lines[" << i << "];\n";
			for(int j = i; j < _ways; ++j)
				output << "\t\t\t\t(block = lines[" << j << "]) : lines[" << i-1 << "];\n";

			output << "\t\t\t\tTRUE : lines[" << i-1 << "];\n";

			output << "\t\t\tesac;\n";
		}
	}
}

std::string MCCacheModel::in() const
{
	std::ostringstream oss;
	switch(_kind)
	{
		case CacheModelKind::YOUNGER_SET:
		case CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR:
			oss << "cache.age != " << _ways;
			break;
		case CacheModelKind::YOUNGER_SET_BITVECTOR:
		case CacheModelKind::YOUNGER_SET_CLOSED_BITVECTOR:
			oss << "count(";
			for(int i = 0; i <= _numberOfBlocks - 1; ++i) {
				if(i != 0)
					oss << ", ";
				oss << "cache.younger_set[" << i << "]";
			}
			oss << ") < " << _ways;
			break;
		case CacheModelKind::MAP_BLOCK_AGE:
			oss << "cache.ages[" << _trackedBlock << "] != " << _ways;
			break;
		case CacheModelKind::QUEUE:
			oss << "(cache.lines[0] = " << _trackedBlock;
			for(int i = 1; i < _ways; ++i)
				oss << " | cache.lines[" << i << "] = " << _trackedBlock;
			oss << ")";
			break;
	}
	return oss.str();
}

std::string MCCacheModel::out() const
{
	std::ostringstream oss;
	switch(_kind)
	{
		case CacheModelKind::YOUNGER_SET:
		case CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR:
			oss << "cache.age = " << _ways;
			break;
		case CacheModelKind::YOUNGER_SET_BITVECTOR:
		case CacheModelKind::YOUNGER_SET_CLOSED_BITVECTOR:
			oss << "count(";
			for(int i = 0; i <= _numberOfBlocks - 1; ++i) {
				if(i != 0)
					oss << ", ";
				oss << "cache.younger_set[" << i << "]";
			}
			oss << ") >= " << _ways;
			break;
		case CacheModelKind::MAP_BLOCK_AGE:
			oss << "cache.ages[" << _trackedBlock << "] = " << _ways;
			break;
		case CacheModelKind::QUEUE:
			oss << "(cache.lines[0] != " << _trackedBlock;
			for(int i = 1; i < _ways; ++i)
				oss << " & cache.lines[" << i << "] != " << _trackedBlock;
			oss << ")";
			break;
	}
	return oss.str();
}

} // namespace lrumc
