#include <lrumc/features.h>

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>
#include <otawa/prog/Process.h>

#include <lruexact/features.h>
#include <lrupreanalysis/features.h>

#include "MCProgramModel.h"
#include "MCProgramModelSlicing.h"

using namespace otawa;

namespace lrumc
{

class MCLBlockSlicingModelBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	MCLBlockSlicingModelBuilder() :
		otawa::BBProcessor(reg),
		_slicedModels(),
		_coll(nullptr),
		_cfgs(nullptr)
	{
	}

protected:

	virtual void setup(WorkSpace *ws) override{
		_coll = icat3::LBLOCKS(ws);
		ASSERT(_coll);
		_cfgs = INVOLVED_CFGS(ws);
		ASSERT(_cfgs);

	}

	virtual void processWorkSpace(WorkSpace* ws) override
	{
		_slicedModels = elm::AllocArray<elm::AllocArray<MCProgramModel*> >(_coll->size());

		for(int i = 0; i < _coll->size(); ++i) {
			_slicedModels[i] = elm::AllocArray<MCProgramModel*>((*_coll)[i].size());
			for(int j = 0; j < (*_coll)[i].size(); ++j)
				_slicedModels[i][j] = nullptr;
		}

		BBProcessor::processWorkSpace(ws);
		LBLOCK_SLICED_PROGRAM_MODEL(ws) = _slicedModels;
	}

	virtual void processBB(WorkSpace* ws, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(ws, bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(ws, bag);
	}

	void processBag(WorkSpace* ws, Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(ws, &bag[i]);
	}

	void processAccess(WorkSpace* ws, icache::Access* access)
	{
		lrupreanalysis::may_must::ACSManager man = lrupreanalysis::may_must::ACSManager(ws);

		icat3::LBlock* lb = icat3::LBLOCK(*access);
		int set = lb->set();
		int index = lb->index();
		if (lruexact::REFINEMENT_CATEGORY(access) != lruexact::RefinementCategory::CLASSIFIED) {
			if(_slicedModels[set][index] == nullptr)
				_slicedModels[set][index] = sliceModelAccordingToLBlock(&man, _coll->A(), GLOBAL_MODEL(ws), (*_coll)[set][index]);
		}
		PROGRAM_MODEL(access) = _slicedModels[set][index];
	}


	AllocArray<AllocArray<MCProgramModel*>> _slicedModels;
	const icat3::LBlockCollection *_coll;
	const CFGCollection *_cfgs;
};

p::declare MCLBlockSlicingModelBuilder::reg = p::init("lrumc::MCLBlockSlicingModelBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
	.require(icat3::LBLOCKS_FEATURE)
	.require(icat3::CATEGORY_FEATURE)
	.require(PROGRAM_MODEL_BUILDING_FEATURE)
	.require(lruexact::REFINEMENT_CATEGORY_FEATURE)
	.provide(LBLOCK_SLICING_PROGRAM_MODEL_FEATURE)
	.make<MCLBlockSlicingModelBuilder>();

p::feature LBLOCK_SLICING_PROGRAM_MODEL_FEATURE("lrumc::LBLOCK_SLICING_PROGRAM_MODEL_FEATURE", p::make<MCLBlockSlicingModelBuilder>());

p::id<elm::AllocArray<elm::AllocArray<MCProgramModel*> > > LBLOCK_SLICED_PROGRAM_MODEL("lrumc::LBLOCK_SLICED_PROGRAM_MODEL");


} // namespace lrumc

