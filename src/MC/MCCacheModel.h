#ifndef LRUMC_MC_CACHE_MODEL_H
#define LRUMC_MC_CACHE_MODEL_H

#include <elm/io.h>

#include <lrumc/features.h>

#include "MCCheck.h"

namespace lrumc
{

class MCCacheModel
{
public:
	MCCacheModel(CacheModelKind kind, int ways, int numberOfBlocks, int trackedBlock);
	void printSMV(elm::io::Output& output, Check check);

	int ways() const {return _ways;}
	std::string in() const;
	std::string out() const;
private:
	CacheModelKind _kind;
	int _ways;
	int _numberOfBlocks;
	int _trackedBlock;
};

} // namespace lrumc

#endif // LRUMC_MC_CACHE_MODEL_H
