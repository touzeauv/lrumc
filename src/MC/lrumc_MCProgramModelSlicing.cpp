#include "MCProgramModelSlicing.h"

#include <lrupreanalysis/features.h>

using namespace otawa;

namespace lrumc {

void recoverACS(lrupreanalysis::may_must::ACSManager* man, const icache::Access* access, const Block* v)
{
	for(Block::EdgeIter e = v->outs(); e(); e++) {
		if(v->isSynth() && v->toSynth()->callee())
			man->start(v->toSynth()->callee()->exit());
		else
			man->start(const_cast<Block*>(v));

		// Perform the access associated to the block
		Bag<icache::Access>& vAccs = *icache::ACCESSES(const_cast<Block*>(v));
		for(int i = 0; i < vAccs.size(); ++i) {
			// Stop when the given access is found
			if(vAccs[i].instruction() == access->instruction())
				return;
			man->update(vAccs[i]);
		}

		// Perform the access associated to the edge
		Bag<icache::Access>& eAccs = *icache::ACCESSES(*e);
		for(int i = 0; i < eAccs.size(); ++i) {
			// Stop when the given access is found
			if(eAccs[i].instruction() == access->instruction())
				return;
			man->update(eAccs[i]);
		}
	}
	ASSERT(false && "access was not found in the given block");
}

MCProgramModel* sliceModelAccordingToSet(const MCProgramModel* model, int set)
{
	auto filter = [set, model] (const MCProgramModelNode* node) {
		bool sameSet = node->access() && icat3::LBLOCK(node->access())->set() == set;
		bool entry = node->id() == model->entry();
		return sameSet || entry;
	};
	return sliceModel(model, filter);
}

MCProgramModel* sliceModelAccordingToLBlock(lrupreanalysis::may_must::ACSManager* man, int ways, const MCProgramModel* model, icat3::LBlock* lblock)
{
	//ASSERT(false && "Bad implementation. Do not use");
	auto filter = [man, ways, model, lblock] (const MCProgramModelNode* node) {
		// Keep the node if it is the model entry
		if(node->id() == model->entry())
			return true;

		// If node is not associated to an access, discard it
		if(node->access() == nullptr)
			return false;

		// Keep the node if it is an access to the given LBlock
		icat3::LBlock* nodeLBlock = icat3::LBLOCK(node->access());
		if((nodeLBlock->set() == lblock->set()) &&
		   (nodeLBlock->index() == lblock->index()))
			return true;

		// Keep the node if the may cache at its entry contains the given LBlock
		recoverACS(man, node->access(), node->block());
		if((nodeLBlock->set() == lblock->set()) &&
		   (man->mayAge(lblock) != ways))
			return true;

		// Otherwise discard it
		return false;
	};
	return sliceModel(model, filter);
}

MCProgramModel* sliceModel(const MCProgramModel* model, std::function<bool(const MCProgramModelNode*)> filter)
{
	MCProgramModel* newModel = new MCProgramModel();

	ExtractionMap extractionMap;

	createNodes(model, filter, newModel, extractionMap);
	createEdges(extractionMap);

	return newModel;
}

void createNodes(
	const MCProgramModel* model,
	std::function<bool(MCProgramModelNode*)> filter,
	MCProgramModel* newModel,
	ExtractionMap& extractionMap)
{
	for(const auto& oldNode : *model) {
		if(filter(oldNode)) {
			MCProgramModelNode::NodeID id = newModel->createNode(oldNode->access(), oldNode->block());
			MCProgramModelNode* newNode = newModel->getNode(id);
			if(oldNode->id() == model->entry())
				newModel->setEntry(id);
			extractionMap.insert(std::make_pair(oldNode, newNode));
		}
		else {
			extractionMap.insert(std::make_pair(oldNode, nullptr));
		}
	}
}

void createEdges(ExtractionMap& extractionMap)
{
	// Perform the reachability analysis
	for(auto& pair : extractionMap) {
		const MCProgramModelNode* oldNode = pair.first;
		MCProgramModelNode* newNode = pair.second;

		// if node is included in the sliced graph
		if (newNode != nullptr) {
			VisitedSet visited;
			std::unordered_set<MCProgramModelNode*> reachableSet = visitNode(oldNode, extractionMap, visited);
			for(MCProgramModelNode* newNodeSuccessor : reachableSet)
				newNode->addSuccessor(newNodeSuccessor);
		}
	}
}

NodeSet visitNode(const MCProgramModelNode* node, const ExtractionMap& extractionMap, VisitedSet& visited)
{
	std::unordered_set<MCProgramModelNode*> reachableNodes;

	visited.insert(node);

	for(const auto& sink : *node) {
		if(extractionMap.at(sink) != nullptr) {
			reachableNodes.insert(extractionMap.at(sink));
			continue;
		}
		if(visited.count(sink) == 1)
			continue;
		std::unordered_set<MCProgramModelNode*> sinkReachable = visitNode(sink, extractionMap, visited);
		reachableNodes.insert(sinkReachable.begin(), sinkReachable.end());
	}
	return reachableNodes;
}

} // namespace lrumc
