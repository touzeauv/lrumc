#ifndef LRUMC_MC_EXCEPTION_H
#define LRUMC_MC_EXCEPTION_H

#include <exception>
#include <string>

namespace lrumc
{

class MCException : public std::exception
{
public:
	MCException(const std::string message);
	virtual ~MCException() throw();
	virtual const char* what() const throw();
private:
	const std::string _message;
};

} // namespace lrumc

#endif // LRUMC_MC_EXCEPTION_H

