#ifndef LRUMC_MC_CHECK_H
#define LRUMC_MC_CHECK_H

namespace lrumc
{

enum class Check
{
	ALWAYS_HIT,
	ALWAYS_MISS
};

} // namespace lrumc

#endif // LRUMC_MC_CHECK_H
