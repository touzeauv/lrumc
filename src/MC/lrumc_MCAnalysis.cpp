#include <chrono>
#include <memory>
#include <queue>
#include <sstream>

#include <elm/io/OutFileStream.h>

#include <otawa/cfg/features.h>
#include <otawa/graph/DiGraph.h>
#include <otawa/hard/Cache.h>
#include <otawa/proc/Processor.h>

#include <lruexact/features.h>
#include <lrumc/stats.h>
#include <lrupreanalysis/features.h>

#include "MCCacheModel.h"
#include "MCCheck.h"
#include "MCException.h"
#include "MCInstance.h"
#include "MCNuxmvCommand.h"
#include "MCProgramModel.h"
#include "MCProgramModelBuilding.h"
#include "MCQueryManager.h"

using namespace otawa;

namespace lrumc
{

class MCAnalysis : public Processor
{
public:
	static otawa::p::declare reg;

	MCAnalysis() :
		otawa::Processor(reg),
		_coll(nullptr),
		_cfgs(nullptr),
		_queryManager(),
		_nuxmvPath(),
		_nuxmvWorkspacePath(),
		_programSlicing(ProgramSlicing::ACCESS_SLICING),
		_cacheModelKind(CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR),
		_instance()
	{
	}

	virtual void configure(const PropList& props) override
	{
		otawa::Processor::configure(props);
		if(props.hasProp(NUXMV_PATH))
			_nuxmvPath = NUXMV_PATH(props);
		if(props.hasProp(NUXMV_WORKSPACE_PATH))
			_nuxmvWorkspacePath = NUXMV_WORKSPACE_PATH(props);
		if(props.hasProp(CACHE_MODEL_KIND))
			_cacheModelKind = CACHE_MODEL_KIND(props);
		if(props.hasProp(PROGRAM_SLICING))
			_programSlicing = PROGRAM_SLICING(props);
	}
protected:

	virtual void setup(WorkSpace *ws) override {
		_coll = icat3::LBLOCKS(ws);
		ASSERT(_coll);
		_cfgs = INVOLVED_CFGS(ws);
		ASSERT(_cfgs);

		_instance = std::unique_ptr<MCInstance>(new MCInstance(_nuxmvPath.chars()));
	}

	virtual void processWorkSpace(WorkSpace*) override {
		auto start = std::chrono::system_clock::now();

		_queryManager = std::unique_ptr<MCQueryManager>(new MCQueryManager(_programSlicing, _cacheModelKind, _coll->sets(), _coll->cache()));

		for(CFGCollection::BlockIter b(_cfgs); b(); ++b) {
			if(b->isBasic()) {
				processBag(*icache::ACCESSES(*b));
			}
			for(Block::EdgeIter succ(b->outs()); succ(); ++succ) {
				processBag(*icache::ACCESSES(*succ));
			}
		}

		while(!_queryManager->isEmpty()) {
			MCQueryManager::Query current = _queryManager->currentTask();

			bool refreshModel = _queryManager->refreshNeeded();

			//FIXME: Compute checkAH checkAM
			processQuery(current, refreshModel);

			_queryManager->nextTask();
		}

		auto end = std::chrono::system_clock::now();
		auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
		if(logFor(LOG_FUN))
			log << "\tModel Checking Analysis running time: " << elapsed.count() << " ms" << io::endl;
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i) {
			lruexact::RefinementCategory cat = lruexact::REFINEMENT_CATEGORY(bag[i]);

			if(cat == lruexact::RefinementCategory::CLASSIFIED) {
				if(icat3::CATEGORY(bag[i]) == icat3::AH)
					lruexact::EXACT_CATEGORY(bag[i]) = lruexact::ExactCategory::AH;
				else if(icat3::CATEGORY(bag[i]) == icat3::AM)
					lruexact::EXACT_CATEGORY(bag[i]) = lruexact::ExactCategory::AM;
				else if(lrupreanalysis::eh_em::DU_CATEGORY(bag[i]) == lrupreanalysis::eh_em::DUCategory::DU)
					lruexact::EXACT_CATEGORY(bag[i]) = lruexact::ExactCategory::DU;
				else
					ASSERT(false);
			}
			else if(cat == lruexact::RefinementCategory::AH_CANDIDATE) {
				lruexact::EXACT_CATEGORY(bag[i]) = lruexact::ExactCategory::DU;
				_queryManager->addTask(std::make_pair(&bag[i], Check::ALWAYS_HIT));
			}
			else if(cat == lruexact::RefinementCategory::AM_CANDIDATE) {
				lruexact::EXACT_CATEGORY(bag[i]) = lruexact::ExactCategory::DU;
				_queryManager->addTask(std::make_pair(&bag[i], Check::ALWAYS_MISS));
			}
			else if(cat == lruexact::RefinementCategory::AH_AM_CANDIDATE) {
				lruexact::EXACT_CATEGORY(bag[i]) = lruexact::ExactCategory::DU;
				_queryManager->addTask(std::make_pair(&bag[i], Check::ALWAYS_MISS));
				_queryManager->addTask(std::make_pair(&bag[i], Check::ALWAYS_HIT));
			}
		}
	}

	void processQuery(MCQueryManager::Query query, bool refreshModel)
	{
		icache::Access* access = query.first;
		Check check = query.second;
		icat3::LBlock* lb = icat3::LBLOCK(*access);
		int set = lb->set();

		const MCProgramModel* programModelPtr = PROGRAM_MODEL(access);
		const MCProgramModel& programModel = *programModelPtr;

		MCCacheModel cacheModel(
			_cacheModelKind,
			_coll->cache()->wayCount(),
			programModel.nbBlocks(),
			programModel.getBlockId(lb));

		if(refreshModel) {
			std::ostringstream modelPath, dotPath;
			modelPath << _nuxmvWorkspacePath.chars() << "/model_set" << set << "_block" << lb->index() << (check == Check::ALWAYS_HIT ? "_always_hit" : "_always_miss")  << ".smv";
			dotPath << _nuxmvWorkspacePath.chars() << "/model_set" << set << "_block" << lb->index() << (check == Check::ALWAYS_HIT ? "_always_hit" : "_always_miss") << ".dot";
			// FIXME: Create workspace if it does not exist

			// FIXME: Build small model using May analysis

			elm::io::OutFileStream smvFile(modelPath.str().c_str());
			elm::io::OutFileStream dotFile(dotPath.str().c_str());
			elm::io::Output outSmv(smvFile);
			elm::io::Output outDot(dotFile);
			cacheModel.printSMV(outSmv, check);
			programModel.printSMV(outSmv);
			programModel.printDOT(outDot);
			smvFile.close();
			dotFile.close();

			MCResetCommand reset;
			MCReadModelCommand read(modelPath.str());
			MCFlattenHierarchyCommand flatten;
			MCEncodeVariablesCommand encode;
			MCBuildModelCommand build;

			if(logFor(LOG_BLOCK))
				log << "\tNew model build: " << modelPath.str().c_str() << "\n";

			reset.readResponse(_instance->execCommand(reset));
			read.readResponse(_instance->execCommand(read));
			flatten.readResponse(_instance->execCommand(flatten));
			encode.readResponse(_instance->execCommand(encode));
			build.readResponse(_instance->execCommand(build));
		}

		MCCheckInvariantCommand checkInvariant(
			programModel,
			cacheModel,
			*access,
			MCCheckInvariantCommand::BlockEnd::ENTRY,
			check);
		if(logFor(LOG_BLOCK)) {
			log << "\tChecking if access (" << *access << ") is " << (check == Check::ALWAYS_HIT ? "Always-Hit" : "Always-Miss") << ".\n";
		}
		checkInvariant.readResponse(_instance->execCommand(checkInvariant));

		if(checkInvariant.hold()) {
			if(logFor(LOG_BLOCK))
				log << "\tAccess (" << *access << ") successfully refined into " << (check == Check::ALWAYS_HIT ? "Always-Hit" : "Always-Miss") << ".\n\n";
			lruexact::EXACT_CATEGORY(*access) = (check == Check::ALWAYS_HIT ? lruexact::ExactCategory::AH : lruexact::ExactCategory::AM);
		}
	}

	virtual void collectStats(WorkSpace* ws) override
	{
		record(new AHCollector(ws));
		record(new AMCollector(ws));
		record(new NCCollector(ws));
	}

	virtual void cleanup(WorkSpace* ws) override
	{
		for(CFGCollection::BlockIter b(_cfgs); b(); ++b)
			cleanupBB(*b);

		if(ws->isProvided(SET_SLICING_PROGRAM_MODEL_FEATURE)) {
			elm::AllocArray<MCProgramModel*>& slicedModels = SET_SLICED_PROGRAM_MODEL(ws).ref();
			for(int i = 0; i < slicedModels.size(); ++i) {
				delete slicedModels[i];
				slicedModels[i] = nullptr;
			}
		}

		delete GLOBAL_MODEL(ws);
		GLOBAL_MODEL(ws) = nullptr;
	}

	void cleanupBB(Block* b)
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			cleanupBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		cleanupBag(bag);
	}

	void cleanupBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			cleanupAccess(&bag[i]);
	}

	void cleanupAccess(icache::Access* access)
	{
		PROGRAM_MODEL(access) = nullptr;
	}

	const icat3::LBlockCollection *_coll;
	const CFGCollection *_cfgs;
	std::unique_ptr<MCQueryManager> _queryManager;
	String _nuxmvPath;
	String _nuxmvWorkspacePath;
	ProgramSlicing _programSlicing;
	CacheModelKind _cacheModelKind;
	std::unique_ptr<MCInstance> _instance;
};

p::declare MCAnalysis::reg = p::init("lrumc::MCAnalysis", Version(1, 0, 0))
	.require(icat3::LBLOCKS_FEATURE)
	.require(icat3::CATEGORY_FEATURE)
	.require(lruexact::REFINEMENT_CATEGORY_FEATURE)
	.require(PROGRAM_MODEL_BUILDING_FEATURE)
	.require(COLLECTED_CFG_FEATURE)
	.require(LOOP_INFO_FEATURE)
	.provide(MODEL_CHECKING_ANALYSIS_FEATURE)
	.provide(lruexact::LRU_CLASSIFICATION_FEATURE)
	.make<MCAnalysis>();
/**
 * Feature performing instruction cache analysis with a model checker.
 */
p::feature MODEL_CHECKING_ANALYSIS_FEATURE("lrumc::MODEL_CHECKING_ANALYSIS_FEATURE", p::make<MCAnalysis>());
p::id<CacheModelKind> CACHE_MODEL_KIND("lrumc::CACHE_MODEL_KIND", CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR);
p::id<ProgramSlicing> PROGRAM_SLICING("lrumc::PROGRAM_SLICING", ProgramSlicing::ACCESS_SLICING);

p::id<elm::String> NUXMV_PATH("lrumc::NUXMV_PATH", "");
p::id<elm::String> NUXMV_WORKSPACE_PATH("lrumc::NUXMV_WORKSPACE_PATH", "/tmp/");

} // namespace lrumc

namespace otawa
{
	template <> void from_string(const elm::string& s, lrumc::CacheModelKind& v)
	{
		if(s == "YOUNGER_SET")
			v = lrumc::CacheModelKind::YOUNGER_SET;
		else if(s == "YOUNGER_SET_BITVECTOR")
			v = lrumc::CacheModelKind::YOUNGER_SET_BITVECTOR;
		else if(s == "YOUNGER_SET_BOUNDED_BITVECTOR")
			v = lrumc::CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR;
		else if(s == "YOUNGER_SET_CLOSED_BITVECTOR")
			v = lrumc::CacheModelKind::YOUNGER_SET_CLOSED_BITVECTOR;
		else if(s == "MAP_BLOCK_AGE")
			v = lrumc::CacheModelKind::MAP_BLOCK_AGE;
		else if(s == "QUEUE")
			v = lrumc::CacheModelKind::QUEUE;
		else
			throw lrumc::MCException("Unknown cache model kind");
	}
	template <> void from_string(const elm::string& s, lrumc::ProgramSlicing& v)
	{
		if(s == "SET_SLICING")
			v = lrumc::ProgramSlicing::SET_SLICING;
		else if(s == "CACHE_BLOCK_SLICING")
			v = lrumc::ProgramSlicing::CACHE_BLOCK_SLICING;
		else if(s == "ACCESS_SLICING")
			v = lrumc::ProgramSlicing::ACCESS_SLICING;
		else
			throw lrumc::MCException("Unknown program slicing");
	}
} // namespace otawa

