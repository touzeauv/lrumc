#include "MCProgramModelNode.h"
#include "MCProgramModel.h"

using namespace otawa;

namespace lrumc {

MCProgramModelNode::MCProgramModelNode(NodeID id, BlockID blockId, const icache::Access* access, const Block* block) :
	_id(id),
	_blockId(blockId),
	_access(access),
	_block(block)
{
}

void MCProgramModelNode::addSuccessor(MCProgramModelNode* node)
{
	_successors.push_back(node);
}

} // namespace lrumc
