#include <lrumc/features.h>

#include <otawa/proc/BBProcessor.h>
#include <otawa/prog/Process.h>
#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>

#include "MCProgramModel.h"
#include "MCProgramModelSlicing.h"

using namespace otawa;

namespace lrumc
{

class MCSetSlicingModelBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	MCSetSlicingModelBuilder() :
		otawa::BBProcessor(reg),
		_coll(nullptr),
		_cfgs(nullptr)
	{
	}

protected:

	virtual void setup(WorkSpace *ws) override
	{
		_coll = icat3::LBLOCKS(ws);
		ASSERT(_coll);
		_cfgs = INVOLVED_CFGS(ws);
		ASSERT(_cfgs);

	}

	virtual void processWorkSpace(WorkSpace* ws) override
	{
		_slicedModels = AllocArray<MCProgramModel*>(_coll->size());
		for(int i = 0; i < _coll->size(); ++i)
			_slicedModels[i] = sliceModelAccordingToSet(GLOBAL_MODEL(ws), i);

		SET_SLICED_PROGRAM_MODEL(ws) = _slicedModels;
		BBProcessor::processWorkSpace(ws);
	}

	virtual void processBB(WorkSpace*, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(bag);
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(&bag[i]);
	}

	void processAccess(icache::Access* access)
	{
		icat3::LBlock* lb = icat3::LBLOCK(*access);
		int set = lb->set();
		PROGRAM_MODEL(access) = _slicedModels[set];
	}


	AllocArray<MCProgramModel*> _slicedModels;
	const icat3::LBlockCollection *_coll;
	const CFGCollection *_cfgs;
};

p::declare MCSetSlicingModelBuilder::reg = p::init("lrumc::MCSetSlicingModelBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
	.require(icat3::LBLOCKS_FEATURE)
	.require(PROGRAM_MODEL_BUILDING_FEATURE)
	.provide(SET_SLICING_PROGRAM_MODEL_FEATURE)
	.make<MCSetSlicingModelBuilder>();

p::feature SET_SLICING_PROGRAM_MODEL_FEATURE("lrumc::SET_SLICING_PROGRAM_MODEL_FEATURE", p::make<MCSetSlicingModelBuilder>());

p::id<elm::AllocArray<MCProgramModel*> > SET_SLICED_PROGRAM_MODEL("lrumc::SET_SLICED_PROGRAM_MODEL");


} // namespace lrumc

