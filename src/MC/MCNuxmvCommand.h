#ifndef LRUMC_MC_NUXMV_COMMAND_H
#define LRUMC_MC_NUXMV_COMMAND_H

#include <string>

#include <otawa/icache/features.h>

#include "MCCheck.h"
#include "MCProgramModel.h"
#include "MCCacheModel.h"

namespace lrumc
{

class MCNuxmvCommand
{
public:
	std::string nuxmvString() const
	{
		return this->getNuxmvString();
	}
	void readResponse(std::string response)
	{
		this->parseResponse(response);
	}
private:
	virtual std::string getNuxmvString() const = 0;
	virtual void parseResponse(std::string response);
};

class MCResetCommand : public MCNuxmvCommand
{
private:
	std::string getNuxmvString() const override;
};

class MCReadModelCommand : public MCNuxmvCommand
{
public:
	MCReadModelCommand(std::string smvModelPath);

private:
	std::string getNuxmvString() const override;
	const std::string _smvModelPath;
};

class MCFlattenHierarchyCommand : public MCNuxmvCommand
{
private:
	std::string getNuxmvString() const override;
};

class MCEncodeVariablesCommand : public MCNuxmvCommand
{
private:
	std::string getNuxmvString() const override;
};

class MCBuildModelCommand : public MCNuxmvCommand
{
private:
	std::string getNuxmvString() const override;
};

class MCCheckInvariantCommand : public MCNuxmvCommand
{
public:
	enum class BlockEnd
	{
		ENTRY,
		EXIT
	};

	MCCheckInvariantCommand(const MCProgramModel& program, const MCCacheModel& cache, const otawa::icache::Access& access, BlockEnd blockEnd, Check check);
	bool hold() const;
private:
	std::string getNuxmvString() const override;
	void parseResponse(std::string response) override;

	std::string _invariant;
	bool _checked;
	bool _hold;
};

} // namespace lrumc

#endif // LRUMC_MC_NUXMV_COMMAND_H
