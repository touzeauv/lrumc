#include <lrumc/features.h>

#include <sstream>

#include <elm/io/OutFileStream.h>
#include <elm/sys/Path.h>
#include <elm/sys/System.h>

#include <otawa/cfg/features.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/proc/BBProcessor.h>
#include <otawa/prog/Process.h>

#include "MCProgramModelBuilding.h"

using namespace otawa;

namespace lrumc
{

class MCModelBuilder : public BBProcessor
{
public:
	static otawa::p::declare reg;

	MCModelBuilder() :
		otawa::BBProcessor(reg),
		_globalModel(),
		_cfgs(nullptr)
	{
	}

	virtual void configure(const PropList& props) override
	{
		otawa::BBProcessor::configure(props);

		if(props.hasProp(NUXMV_WORKSPACE_PATH))
			_nuxmvWorkspacePath = NUXMV_WORKSPACE_PATH(props);
	}
protected:

	virtual void setup(WorkSpace *ws) override
	{
		_cfgs = INVOLVED_CFGS(ws);
		ASSERT(_cfgs);

	}

	virtual void processWorkSpace(WorkSpace* ws) override
	{
		_globalModel = buildProgramModel(_cfgs);
		GLOBAL_MODEL(ws) = _globalModel;

		std::ostringstream dotPath, smvPath;
		dotPath << _nuxmvWorkspacePath.chars() << "/globalModel.dot";
		smvPath << _nuxmvWorkspacePath.chars() << "/globalModel.smv";
		elm::io::OutFileStream dotFile(dotPath.str().c_str());
		elm::io::OutFileStream smvFile(smvPath.str().c_str());
		elm::io::Output outDot(dotFile);
		elm::io::Output outSmv(smvFile);
		_globalModel->printDOT(outDot);
		_globalModel->printSMV(outSmv);
		dotFile.close();
		smvFile.close();

		BBProcessor::processWorkSpace(ws);
	}

	virtual void processBB(WorkSpace*, CFG*, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
			Edge* e = *edgeIter;
			Bag<icache::Access>& bag = icache::ACCESSES(e).ref();
			processBag(bag);
		}

		Bag<icache::Access>& bag = icache::ACCESSES(bb).ref();
		processBag(bag);
	}

	void processBag(Bag<icache::Access>& bag)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(&bag[i]);
	}

	void processAccess(icache::Access* access)
	{
		PROGRAM_MODEL(access) = _globalModel;
	}


	MCProgramModel* _globalModel;
	const CFGCollection *_cfgs;
	String _nuxmvWorkspacePath;
};

p::declare MCModelBuilder::reg = p::init("lrumc::MCModelBuilder", Version(1, 0, 0))
	.require(icache::ACCESSES_FEATURE)
	.require(icat3::LBLOCKS_FEATURE)
	.provide(PROGRAM_MODEL_BUILDING_FEATURE)
	.make<MCModelBuilder>();

p::feature PROGRAM_MODEL_BUILDING_FEATURE("lrumc::PROGRAM_MODEL_BUILDING_FEATURE", p::make<MCModelBuilder>());

p::id<MCProgramModel*> GLOBAL_MODEL("lrumc::GLOBAL_MODEL", nullptr);
p::id<MCProgramModel*> PROGRAM_MODEL("lrumc::PROGRAM_MODEL", nullptr);


} // namespace lrumc

