#ifndef LRUMC_MC_PROGRAM_MODEL_SLICING_H_
#define LRUMC_MC_PROGRAM_MODEL_SLICING_H_

#include <unordered_set>
#include <unordered_map>

#include "MCProgramModel.h"

namespace lrumc {

using NodeSet = std::unordered_set<MCProgramModelNode*>;
using VisitedSet = std::unordered_set<const MCProgramModelNode*>;
using ReachabilityMap = std::unordered_map<const MCProgramModelNode*, NodeSet>;
using ExtractionMap = std::unordered_map<const MCProgramModelNode*, MCProgramModelNode*>;
using VisitMap = std::unordered_map<const MCProgramModelNode*, bool>;

void createNodes(
	const MCProgramModel* model,
	std::function<bool(MCProgramModelNode*)> filter,
	MCProgramModel* newModel,
	ExtractionMap& extractionMap);

void createEdges(ExtractionMap& extractionMap);

NodeSet visitNode(
	const MCProgramModelNode* node,
	const ExtractionMap& extractionMap,
	VisitedSet& visited);

} // namespace lrumc

#endif // LRUMC_MC_PROGRAM_MODEL_SLICING_H_
