#ifndef LRUMC_MC_INSTANCE_H
#define LRUMC_MC_INSTANCE_H

#include <string>
#include <unistd.h>

#include "MCNuxmvCommand.h"

//                ///////////////
//                //           //
//                //  PROCESS  //
//                //  in  out  //
//                ///////////////
//                  0 ^    | 1
//                    |    |
//                    |    |
//                    |    |
//                    |    |
//  fdChildToParent   |    |  dfParentToChild
//                    |    |
//                    |    |
//                    |    |
//                    |    |
//                    |    |
//                  1 |    v 0
//                ///////////////
//                //  out  in  //
//                //    MC     //
//                //           //
//                ///////////////

namespace lrumc
{

class MCInstance
{
public:
	MCInstance(std::string nuxmvPath);
	~MCInstance();
	MCInstance(const MCInstance& other) = delete;
	MCInstance& operator=(const MCInstance& other) = delete;

	MCInstance(MCInstance&& other);
	MCInstance& operator=(MCInstance&& other);

	std::string execCommand(const MCNuxmvCommand& command) const;
private:
	void sendCommand(std::string command) const;
	std::string readResponse() const;
	bool _open;
	pid_t _modelCheckerPid;
	int _fdParentToChild[2], _fdChildToParent[2];
};

} // namespace lrumc

#endif // LRUMC_MC_INSTANCE_H

