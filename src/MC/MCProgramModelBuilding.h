#ifndef LRUMC_MC_PROGRAM_MODEL_BUILDING_H_
#define LRUMC_MC_PROGRAM_MODEL_BUILDING_H_

#include <unordered_map>

#include <otawa/cfg/features.h>

#include "MCProgramModel.h"

namespace lrumc {

using BlockMap = std::unordered_map<const otawa::Block*, std::pair<MCProgramModelNode*, MCProgramModelNode*> >;
using EdgeMap = std::unordered_map<const otawa::Edge*, std::pair<MCProgramModelNode*, MCProgramModelNode*> >;

void buildBasicBlockModel(MCProgramModel* model, const otawa::BasicBlock* bb, BlockMap& blockMap);
void buildSynthBlockModel(MCProgramModel* model, const otawa::SynthBlock* sb, BlockMap& blockMap);
void buildEndBlockModel(MCProgramModel* model, const otawa::Block* b, BlockMap& blockMap);
void buildEdgeModel(MCProgramModel* model, const otawa::Edge* e, EdgeMap& edgeMap);
std::pair<MCProgramModelNode*, MCProgramModelNode*> buildAccessBagModel(MCProgramModel* model, const otawa::Bag<otawa::icache::Access>& bag, const otawa::Block* block);
void connectAccessModels(const otawa::CFG* cfg, const BlockMap& blockMap, const EdgeMap& edgeMap);
void connectCFGModels(const otawa::CFGCollection* cfgs, const BlockMap& blockMap);

} // namespace lrumc

#endif // LRUMC_MC_PROGRAM_MODEL_BUILDING_H_
