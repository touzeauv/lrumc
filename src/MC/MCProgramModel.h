#ifndef LRUMC_MC_PROGRAM_MODEL_H_
#define LRUMC_MC_PROGRAM_MODEL_H_

#include <functional>
#include <vector>
#include <map>

#include <elm/io/Output.h>

#include <otawa/hard/Cache.h>
#include <otawa/cfg/features.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>

#include <lrupreanalysis/features.h>

#include "MCProgramModelNode.h"

namespace lrumc {

class MCProgramModel
{
public:
	using NodeID = MCProgramModelNode::NodeID;
	using BlockID = MCProgramModelNode::BlockID;

	using NodeMap = std::map<const otawa::icache::Access*, NodeID>;
	using BlockMap = std::map<const otawa::icat3::LBlock*, BlockID>;

	using node_iterator = std::vector<MCProgramModelNode*>::iterator;
	using node_const_iterator = std::vector<MCProgramModelNode*>::const_iterator;

	~MCProgramModel();

	NodeID createNode(const otawa::icache::Access* access, const otawa::Block* block);
	const MCProgramModelNode* getNode(NodeID id) const;
	MCProgramModelNode* getNode(NodeID id);
	NodeID getAccessId(const otawa::icache::Access* access) const;
	BlockID getBlockId(const otawa::icat3::LBlock* block) const;

	void setEntry(NodeID entry);
	NodeID entry() const;

	int nbBlocks() const;

	void printDOT(elm::io::Output& output) const;
	void printSMV(elm::io::Output& output) const;

	node_iterator begin();
	node_iterator end();
	node_const_iterator begin() const;
	node_const_iterator end() const;

private:
	BlockID createBlockIfNeeded(const otawa::icat3::LBlock* lblock);

	std::vector<MCProgramModelNode*> _nodes;
	std::vector<const otawa::icat3::LBlock*> _blocks;

	NodeMap _nodeIdMap;
	BlockMap _blockIdMap;

	NodeID _entry;
};

MCProgramModel* buildProgramModel(const otawa::CFGCollection* cfgs);

MCProgramModel* sliceModelAccordingToSet(const MCProgramModel* model, int set);

MCProgramModel* sliceModelAccordingToLBlock(lrupreanalysis::may_must::ACSManager* man, int ways, const MCProgramModel* model, otawa::icat3::LBlock* lblock);

MCProgramModel* sliceModel(const MCProgramModel* model, std::function<bool(const MCProgramModelNode*)> filter);

} // namespace lrumc

#endif // LRUMC_MC_PROGRAM_MODEL_H_
