#include "MCNuxmvCommand.h"

#include <fstream>
#include <iostream>
#include <sstream>

#include "MCException.h"

using namespace otawa;

namespace lrumc
{

void MCNuxmvCommand::parseResponse(std::string response)
{
	if (response != "nuXmv > ") {
		std::string error = "Unexpected response: ";
		error += response;
		throw MCException(error);
	}
}

std::string MCResetCommand::getNuxmvString() const
{
	return "reset\n";
}

MCReadModelCommand::MCReadModelCommand(std::string smvModelPath) :
	MCNuxmvCommand(),
	_smvModelPath(smvModelPath)
{
	std::ifstream test(smvModelPath.c_str());
	if(!test.good()) {
		std::string error = "Cannot find file ";
		error += smvModelPath;
		throw MCException(error);
	}
}

std::string MCReadModelCommand::getNuxmvString() const
{
	std::string command("read_model -i ");
	command += _smvModelPath;
	command += "\n";
	return command;
}

std::string MCFlattenHierarchyCommand::getNuxmvString() const
{
	return "flatten_hierarchy\n";
}

std::string MCEncodeVariablesCommand::getNuxmvString() const
{
	return "encode_variables\n";
}

std::string MCBuildModelCommand::getNuxmvString() const
{
	return "build_model\n";
}

MCCheckInvariantCommand::MCCheckInvariantCommand(
		const MCProgramModel& program,
		const MCCacheModel& cache,
		const icache::Access& access,
		BlockEnd blockEnd,
		Check check) :
	MCNuxmvCommand(),
	_invariant(),
	_checked(false),
	_hold(false)
{
	std::ostringstream ossInvariant;

	if(blockEnd == BlockEnd::ENTRY)
		ossInvariant << "next(state) != ";
	else
		ossInvariant << "state != ";

	ossInvariant << program.getAccessId(&access);
	ossInvariant << " | ";

	if(check == Check::ALWAYS_HIT)
		ossInvariant << cache.in();
	else if (check == Check::ALWAYS_MISS)
		ossInvariant << cache.out();

	_invariant = ossInvariant.str();
}

std::string MCCheckInvariantCommand::getNuxmvString() const
{
	std::string command("check_invar -p \"");
	command += _invariant;
	command += "\"\n";
	return command;
}

void MCCheckInvariantCommand::parseResponse(std::string response)
{
	_checked = true;

	unsigned long firstEndl = response.find("\n");
	if(firstEndl == std::string::npos) {
		firstEndl = response.length();
	}

	if(firstEndl > 7 && response.compare(firstEndl - 7, 7, "is true") == 0)
		_hold = true;
	else if(firstEndl > 8 && response.compare(firstEndl - 8, 8, "is false") == 0)
		_hold = false;
	else {
		std::string error = "Error when checking invariant \"";
		error += _invariant;
		error += "\": ";
		error += response;
		throw MCException(error);
	}
}

bool MCCheckInvariantCommand::hold() const
{
	if(!_checked) {
		std::string error = "Invariant not checked yet";
		throw MCException(error);
	}
	return _hold;
}

} // namespace lrumc
