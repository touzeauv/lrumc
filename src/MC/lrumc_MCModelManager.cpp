#include "MCModelManager.h"

#include <lrumc/features.h>

using namespace otawa;

namespace lrumc
{

MCModelManager::MCModelManager(ProgramSlicing programSlicing, int sets, const MCProgramModel* globalModel, const hard::Cache* cache) :
	_programSlicing(programSlicing),
	_globalModel(globalModel),
	_setModels(sets, nullptr),
	_cache(cache)
{
}

MCModelManager::~MCModelManager()
{
	delete _globalModel;
	for(auto model : _setModels)
		delete model;
}

MCProgramModel& MCModelManager::getModel(int set)
{
	if(_setModels[set] == nullptr)
		_setModels[set] = sliceModelAccordingToSet(_globalModel, _cache, set);
	return *(_setModels[set]);
}

MCProgramModel& MCModelManager::getModel(icache::Access& access)
{
	icat3::LBlock* lb = icat3::LBLOCK(access);
	int set = lb->set();

	//FIXME: Simplify according to the given access !
	if(ACCESS_MODEL(access) == nullptr)
		ACCESS_MODEL(access) = &getModel(set);

	MCProgramModel* model = ACCESS_MODEL(access);
	return *model;
}

otawa::p::id<MCProgramModel*> ACCESS_MODEL("lrumc::ACCESS_MODEL", nullptr);

} // namespace lrumc

