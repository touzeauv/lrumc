#include "MCQueryManager.h"

#include <lrumc/features.h>

using namespace otawa;

namespace lrumc
{

bool MCQueryManager::QueryOrder::operator() (const Query& lhs, const Query& rhs)
{
	// FIXME: Adapt the order to the models
	icache::Access* laccess = lhs.first;
	icache::Access* raccess = rhs.first;
	icat3::LBlock* llb = icat3::LBLOCK(*laccess);
	icat3::LBlock* rlb = icat3::LBLOCK(*raccess);
	int lset = llb->set();
	int rset = rlb->set();
	return (lset < rset || (lset == rset && laccess < raccess));
}

MCQueryManager::MCQueryManager(
		ProgramSlicing slicing,
		CacheModelKind cacheModelKind,
		int sets,
		const hard::Cache* cache) :
	_worklist(),
	_slicing(slicing),
	_cacheModelKind(cacheModelKind),
	_setModels(sets, nullptr),
	_cache(cache),
	_lastTask(nullptr)
{
}

MCQueryManager::~MCQueryManager()
{
	for(auto model : _setModels)
		delete model;
}

void MCQueryManager::addTask(Query query)
{
	_worklist.push(query);
}

bool MCQueryManager::isEmpty() const
{
	return _worklist.empty();
}

MCQueryManager::Query MCQueryManager::currentTask() const
{
	ASSERT(!_worklist.empty());
	return _worklist.top();
}

bool MCQueryManager::refreshNeeded() const
{
	if(_lastTask == nullptr)
		return true;

	if(_cacheModelKind == CacheModelKind::YOUNGER_SET ||
	   _cacheModelKind == CacheModelKind::YOUNGER_SET_BITVECTOR ||
	   _cacheModelKind == CacheModelKind::YOUNGER_SET_CLOSED_BITVECTOR ||
	   _cacheModelKind == CacheModelKind::YOUNGER_SET_BOUNDED_BITVECTOR)
		return true;

	icat3::LBlock* lbCurrent;
	icat3::LBlock* lbLast;

	switch(_slicing)
	{
		case ProgramSlicing::SET_SLICING:
			lbCurrent = icat3::LBLOCK(*(currentTask().first));
			lbLast = icat3::LBLOCK(*_lastTask);
			return lbCurrent->set() != lbLast->set();
		case ProgramSlicing::CACHE_BLOCK_SLICING:
			lbCurrent = icat3::LBLOCK(*(currentTask().first));
			lbLast = icat3::LBLOCK(*_lastTask);
			return lbCurrent != lbLast;
		case ProgramSlicing::ACCESS_SLICING:
			return true;
	}
	ASSERT(0 && "Unknown Program Slicing");
	return true;
}

void MCQueryManager::nextTask()
{
	_lastTask = _worklist.top().first;
	_worklist.pop();
}


otawa::p::id<MCProgramModel*> ACCESS_MODEL("lrumc::ACCESS_MODEL", nullptr);

} // namespace lrumc
