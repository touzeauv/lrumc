#include "MCInstance.h"

#include "MCException.h"

#include <cstring>
#include <sstream>

#include <iostream>
#include <unistd.h>
#include <sys/stat.h>

//#include <csignal>

#define PIPE_READ_END 0
#define PIPE_WRITE_END 1

namespace lrumc
{

MCInstance::MCInstance(std::string nuxmvPath) :
	_open(true),
	_fdParentToChild(),
	_fdChildToParent()
{
	struct stat buffer {};
	if(stat (nuxmvPath.c_str(), &buffer) != 0) {
		throw MCException(std::string("Cannot find the model checker: ").append(strerror(errno)));
	}

	if(pipe(_fdParentToChild) != 0 || pipe(_fdChildToParent) != 0) {
		throw MCException("Failed to pipe the model checker");
	}

	pid_t childPid = fork();

	if(childPid < 0) {
		throw MCException("Fork failed");
	}
	else if(childPid == 0) { // In the child process
		// Close pipe from parent
		if(dup2(_fdParentToChild[PIPE_READ_END], 0) != 0 ||	// Set standard input to read end
		   close(_fdParentToChild[PIPE_READ_END]) != 0 ||	// Already duplicate
		   close(_fdParentToChild[PIPE_WRITE_END]) != 0)	// Unused
			throw MCException("Child: failed to set up standard input");

		// Close pipe to parent
		if(dup2(_fdChildToParent[PIPE_WRITE_END], 1) != 1 ||	// Set standard output to write end
		   close(_fdChildToParent[PIPE_WRITE_END]) != 0 ||		// Already duplicate
		   close(_fdChildToParent[PIPE_READ_END]) != 0)			// Unused
			throw MCException("Child: failed to set up standard output");

		if(dup2(1, 2) != 2)
			throw MCException("Child: failed to redirect output");

		// Change the executable image of the process (launch the model checker)
		const char* const argv[] = {nuxmvPath.c_str(), "-int", NULL};
		execv(nuxmvPath.c_str(), const_cast<char* const*>(argv));
		throw MCException("Failed to execute Model Checker");
	}
	else { // In the parent process
		_modelCheckerPid = childPid;

		// Close unused file descriptors
		close(_fdParentToChild[PIPE_READ_END]);
		close(_fdChildToParent[PIPE_WRITE_END]);
	}
	readResponse();
}

MCInstance::~MCInstance()
{
	if(_open) {
		sendCommand("quit\n");
		close(_fdParentToChild[PIPE_WRITE_END]);
		close(_fdChildToParent[PIPE_READ_END]);
	}
}

MCInstance::MCInstance(MCInstance&& other)
{
	_modelCheckerPid = other._modelCheckerPid;
	_fdChildToParent[0] = other._fdChildToParent[0];
	_fdChildToParent[1] = other._fdChildToParent[1];
	_fdParentToChild[0] = other._fdParentToChild[0];
	_fdParentToChild[1] = other._fdParentToChild[1];

	other._open = false;
}

MCInstance& MCInstance::operator=(MCInstance&& other)
{
	_modelCheckerPid = other._modelCheckerPid;
	_fdChildToParent[0] = other._fdChildToParent[0];
	_fdChildToParent[1] = other._fdChildToParent[1];
	_fdParentToChild[0] = other._fdParentToChild[0];
	_fdParentToChild[1] = other._fdParentToChild[1];

	other._open = false;

	return *this;
}

std::string MCInstance::execCommand(const MCNuxmvCommand& command) const
{
	sendCommand(command.nuxmvString());
	return readResponse();
}

void MCInstance::sendCommand(std::string command) const
{
	// Send command
	int nbytes = command.length();
	int result = write(_fdParentToChild[PIPE_WRITE_END], command.c_str(), nbytes);
	if( result != nbytes) {
		std::ostringstream oss;
		oss << "Short write to child on command : \"" << command << "\"\n.";
		oss << "Write " << result << " bytes on fd " << _fdParentToChild[PIPE_WRITE_END] << " instead of " << nbytes << " bytes.";
		oss << "Error is : " << strerror(errno);
		throw MCException(oss.str().c_str());
	}
}

std::string MCInstance::readResponse() const
{
	// Wait until response is complete (new prompt)

	char readBuffer[80];
	std::string response = "";
	while(true) {
		int bytesRead = read(_fdChildToParent[PIPE_READ_END], readBuffer, sizeof(readBuffer)-1);
		if(bytesRead < 0)
			throw MCException("Parent: Incorrect reading");
		readBuffer[bytesRead] = '\0';
		response += readBuffer;
		int length = response.length();

		if(length < 8)
			continue;

		std::string end = response.substr(length - 8, 7);
		if(end == "nuXmv >")
			break;
	}
	return response.substr(0, response.length()-9);
}

} // namespace lrumc
