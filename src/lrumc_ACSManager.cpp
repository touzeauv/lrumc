#include <lrumc/ACSManager.h>

#include <otawa/icat3/features.h>
#include <lrumc/features.h>

#include "CacheState.h"

using namespace otawa;

namespace lrumc
{

ACSManager::ACSManager(WorkSpace* ws) :
	_underlyingManager(ws),
	_coll(**icat3::LBLOCKS(ws)),
	_b(nullptr)
{
	_states = new CacheState*[_coll.sets()];
	array::set(_states, _coll.sets(), null<CacheState>());
	_hasExistHit = ws->isProvided(lrumc::EXIST_HIT_ANALYSIS_FEATURE);
	_hasExistMiss = ws->isProvided(lrumc::EXIST_MISS_ANALYSIS_FEATURE);
}

ACSManager::~ACSManager()
{
	for(int i = 0; i < _coll.sets(); ++i)
		if(_states[i] != nullptr)
			delete _states[i];

	delete[] _states;
}

void ACSManager::start(otawa::Block* b)
{
	ASSERT(b);
	_b = b;
	_underlyingManager.start(b);
	_used.clear();
}

void ACSManager::update(const icache::Access& acc)
{
	icat3::LBlock* lb = icat3::LBLOCK(acc);
	CacheState& state = use(lb->set());
	if(_hasExistHit)
		state._existHitDomain.update(acc, *state._existHitState, _underlyingManager);
	if(_hasExistMiss)
		state._existMissDomain.update(acc, *state._existMissState, _underlyingManager);
	_underlyingManager.update(acc);
}

int ACSManager::mustAge(const icat3::LBlock* lb)
{
	return _underlyingManager.mustAge(lb);
}

int ACSManager::mayAge(const icat3::LBlock* lb)
{
	return _underlyingManager.mayAge(lb);
}

int ACSManager::existHitAge(const icat3::LBlock* lb)
{
	if(!_hasExistHit)
		return 0; // FIXME
	else
		return use(lb->set())._existHitState->get(lb->index());
}

int ACSManager::existMissAge(const icat3::LBlock* lb)
{
	if(!_hasExistMiss)
		return 0;
	else
		return use(lb->set())._existMissState->get(lb->index());
}

void ACSManager::print(const icat3::LBlock* lb, Output& out)
{
	CacheState& state = use(lb->set());
	if(_hasExistHit) {
		out << "{EH: ";
		state._existHitDomain.print(*state._existHitState, out);
	}
	if(_hasExistMiss) {
		out << ", EM";
		state._existMissDomain.print(*state._existMissState, out);
	}
	out << "}";
}

CacheState& ACSManager::use(int set)
{
	if(!_used.contains(set)) {
		if(_states[set] == nullptr)
			_states[set] = new CacheState(_coll, set);
		if(_hasExistHit) {
			if(_states[set]->_existHitState == nullptr)
				_states[set]->_existHitState = new ExistHitDomain::t;
			_states[set]->_existHitDomain.copy(*_states[set]->_existHitState, (*EXIST_HIT_IN(_b))[set]);
		}
		if(_hasExistMiss) {
			if(_states[set]->_existMissState == nullptr)
				_states[set]->_existMissState = new ExistMissDomain::t;
			_states[set]->_existMissDomain.copy(*_states[set]->_existMissState, (*EXIST_MISS_IN(_b))[set]);
		}
		_used.add(set);
	}

	return *_states[set];
}

} // namespace lrumc
